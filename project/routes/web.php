<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/film', function () {
    $films =[
        ['name'=>'The Shining','category'=>'Horror'],
        ['name'=>'Black Widow','category'=>'Action'],
        ['name'=>'Space Jam','category'=>'Cartoon'],
    ];
    return view('film', ['movies'=>$films]);
})->name('film');

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/serietv', function () {
    $serietv =[
        ['name'=>'How i met you mother','category'=>'Comedy'],
        ['name'=>'La casa di carta','category'=>'Action'],
        ['name'=>'Mr. Robot','category'=>'Thriller'],
    ];
    return view('serietv', ['serie'=>$serietv]);
})->name('serieTV');


// Route::get('/film', function () {
//     return view('film');
// });